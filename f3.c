int f3(unsigned int i)
{
  signed int ret = 0;
  while(i!=0)
  {
	if(i&1)
	{
		ret++;
	}
	i=i>>1;
  }
  
  return -1*ret;   
}
